function getInfo()
	return {
		tooltip = "Reclaim area",
		parameterDefs = {
			{ 
				name = "reclaim_unitIDs", -- units to load
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "{<unitID>}",
			},
			{ 
				name = "fark_units", -- loading unit formation
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "{<farkUnit>}",
			},
		}
	}
end

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitTransporter = Spring.GetUnitTransporter
local SpringGetUnitIsTransporting = Spring.GetUnitIsTransporting
local SpringGetUnitsInSphere = Spring.GetUnitsInSphere
local SpringGetUnitVelocity = Spring.GetUnitVelocity 
local GetGroundHeight = Spring.GetGroundHeight
---------------- functions --------------

----------------------- main function -----------

function Run(self, units, parameter)
	local reclaim_unitIDs = parameter.reclaim_unitIDs -- table with unitID
	local fark_units = parameter.fark_units -- unitID
	
	
		
	-- validation
	if reclaim_unitIDs == nil then
		Logger.warn("Ahry.reclaim", "reclaim_unitIDs cannot be nil.") 
		return FAILURE		
	end
	for i=1, #fark_units do
		local fark = fark_units[i]
		local unitDefId = Spring.GetUnitDefID(fark)
		if not UnitDefs[unitDefId] or UnitDefs[unitDefId].name ~= "armfark" then
			Logger.warn("Ahry.reclaim", "Unit [" .. fark .. "] is not fark. It is " .. UnitDefs[unitDefId]) 
			return FAILURE
		end
	end
	
	-- pick the spring command implementing the unload unit
	local cmdID = CMD.RECLAIM
	for j=1, #fark_units do
		local fark = fark_units[j]
		if Sensors.ahry.unit_alive(fark) then
			local command = Spring.GetUnitCommands(fark, 1)
			local command = command and command[1]
			if not command or command.id ~= CMD.RECLAIM or not Sensors.ahry.cointains(reclaim_unitIDs,command.params[1]) then
				local modifier = {}
				for i=1, #reclaim_unitIDs do
					local unitID = reclaim_unitIDs[i]
					if Sensors.ahry.unit_alive(unitID) then
						SpringGiveOrderToUnit(fark, cmdID, {unitID}, modifier)
						modifies = {"shift"}
					end
				end
			end
		end
	end
	
	for i=1, #reclaim_unitIDs do
		local unitID = reclaim_unitIDs[i]
		if Sensors.ahry.unit_alive(unitID) then
			return RUNNING
		end
	end
	
	return SUCCESS
end