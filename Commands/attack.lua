function getInfo()
	return {
		tooltip = "Load units",
		parameterDefs = {
			{ 
				name = "attacking_units", -- units to load
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "<units to load>",
			},
			{ 
				name = "unit_to_attack", -- loading unit formation
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "<loading unit>",
			},
		}
	}
end

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitVelocity = Spring.GetUnitVelocity

-- function
function distance(from, to)
	local dx = to.x - from.x
	local dy = to.y - from.y
	local dz = to.z - from.z
	return math.sqrt(dx*dx+dy*dy+dz*dz)
end


function Run(self, units, parameter)
	local attacking_units = parameter.attacking_units -- table with unitID
	local unit_to_attack = parameter.unit_to_attack -- table with unitID

	if not unit_to_attack or not Sensors.ahry.unit_alive(unit_to_attack) then
		for i=1, #attacking_units do
			local unitID = attacking_units[i]
			local command = Spring.GetUnitCommands(unitID, 1)
			local command = command and command[1]
			
			if command and Sensors.ahry.unit_alive(unitID) and command.id == CMD.ATTACK then
				SpringGiveOrderToUnit(unitID, CMD.STOP, {}, {})
			end
		end
		return SUCCESS
	end
		
	-- validation
	
	
	-- pick the spring command implementing the load unit
	local x,y,z = SpringGetUnitPosition(unit_to_attack)
	local enemy_pos = {x=x, y=y, z=z}
	local orderedUnit = nil
	for i=1, #attacking_units do
		local unitID = attacking_units[i]
		if Sensors.ahry.unit_alive(unitID) then
			orderedUnit = unitID
			local vel_x, vel_y, vel_z, vel_length = SpringGetUnitVelocity(unitID)
			local unit_range = Sensors.ahry.enemy_unit_range(unitID)
			if enemy_pos and distance(enemy_pos, unit_range.position) < unit_range.range then
				SpringGiveOrderToUnit(unitID, CMD.ATTACK, {x,y,z}, {})
			else
				SpringGiveOrderToUnit(unitID, CMD.ATTACK, {unit_to_attack}, {})
			end
		end
	end
	return RUNNING
end
