function getInfo()
	return {
		tooltip = "Load units",
		parameterDefs = {
			{ 
				name = "repairing_unit", -- units to load
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "<repairing_unit>",
			},
			{ 
				name = "fark_units", -- loading unit formation
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "{<fark_unit>}",
			},
		}
	}
end

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitTransporter = Spring.GetUnitTransporter
local SpringGetUnitIsTransporting = Spring.GetUnitIsTransporting
local SpringGetUnitsInSphere = Spring.GetUnitsInSphere
local SpringGetUnitVelocity = Spring.GetUnitVelocity 
local GetGroundHeight = Spring.GetGroundHeight
local SpringGetUnitDefID = Spring.GetUnitDefID


----------------------- main function -----------

function Run(self, units, parameter)
	local repairing_unit = parameter.repairing_unit -- table with unitID
	local fark_units = parameter.fark_units -- unitID
	
	if #fark_units == 0 or not Sensors.ahry.unit_alive(repairing_unit) then
		return SUCCESS
	end
		
	-- validation
	if repairing_unit == nil then
		Logger.warn("Ahry.repair", "repairing_unit cannot be nil.") 
		return FAILURE		
	end
	if fark_units == nil then
		Logger.warn("Ahry.repair", "fark_units cannot be nil.") 
		return FAILURE		
	end
	for i=1, #fark_units do
		local unitID = fark_units[i]
		if unitID and Sensors.ahry.unit_alive(unitID) then
			local unitDefId = SpringGetUnitDefID(unitID)
			if unitDefId and (not UnitDefs[unitDefId] or UnitDefs[unitDefId].name ~= "armfark") then
				Logger.warn("Ahry.repair", "Unit [" .. unitID .. "] is not fark. It is " .. UnitDefs[unitDefId].name) 
				return FAILURE
			end
		end
	end
	
	-- pick the spring command implementing the unload unit
	local cmdID = CMD.REPAIR

	local ordered_unit = nil
	for i=1, #fark_units do
		local unitID = fark_units[i]
		if Sensors.ahry.unit_alive(unitID) then
			ordered_unit = unitID
			SpringGiveOrderToUnit(unitID, cmdID, {repairing_unit}, {})
		end
	end
	
	if not ordered_unit or self:UnitIdle(ordered_unit) then
		return SUCCESS
	end
	return RUNNING
end
