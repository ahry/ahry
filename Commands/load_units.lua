function getInfo()
	return {
		tooltip = "Load units",
		parameterDefs = {
			{ 
				name = "units_to_load", -- units to load
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "<units to load>",
			},
			{ 
				name = "loading_unit", -- loading unit formation
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "<loading unit>",
			},
		}
	}
end

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitTransporter = Spring.GetUnitTransporter

function Run(self, units, parameter)
	local units_to_load = parameter.units_to_load -- table with unitID
	local loading_unit = parameter.loading_unit -- table with unitID
	
	
		
	-- validation
	for _, unitID in pairs(units_to_load) do
		local unitDefId = Spring.GetUnitDefID(unitID)
		if not UnitDefs[unitDefId] or UnitDefs[unitDefId].cantBeTransported then
			Logger.warn("Ahry.load_unit", "Unit [" .. unitID .. "] cannot be transported.") 
			return FAILURE
		end
	end
	local unitDefId = Spring.GetUnitDefID(loading_unit)
	if not UnitDefs[unitDefId] or not UnitDefs[unitDefId].isTransport then
		Logger.warn("Ahry.load_unit", "Unit [" .. loading_unit .. "] is not transport.") 
		return FAILURE
	end
	
	-- pick the spring command implementing the load unit
	local cmdID = CMD.LOAD_UNITS
	
	for _, unitID in pairs(units_to_load) do
		if not SpringGetUnitTransporter(unitID) then
			SpringGiveOrderToUnit(loading_unit, cmdID, {unitID}, {})
			return RUNNING
		end
	end
	return SUCCESS
end