function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move to defined position",
		parameterDefs = {
			{ 
				name = "unit", -- units to positions in formation
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "<units to positions>",
			},
			{ 
				name = "path",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "<path>",
			},
			{ 
				name = "fight",
				variableType = "expression",
				componentType = "checkBox",
				defaultValue = "false",
			}
		}
	}
end

local CLOSE_DISTANCE = 500

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitVelocity = Spring.GetUnitVelocity

---------------- functions --------------
function distance(from_x, from_z, to_x, to_z)
	local dx = from_x - to_x
	local dz = from_z - to_z
	
	return math.sqrt(dx*dx+dz*dz)
end

function path_index(unitID, path)
	local index =  1
	
	local x,_,z = SpringGetUnitPosition(unitID)
	local nearest_distance = distance(x, z, path[index].x, path[index].z)
	for i=index + 1, #path do
		local new_distance = distance(x, z, path[i].x, path[i].z)
		
		if nearest_distance >= new_distance then
			nearest_distance = new_distance
			index = i
		end
	end
	return math.min(#path, index + 1)
end

function Run(self, units, parameter)
	local unitID = parameter.unit -- table
	
	if unitID == nil then
		return SUCCESS
	end
	
	if not Sensors.ahry.unit_alive(unitID) then
		return FAILURE
	end
	
	local path = parameter.path -- table or Vec3
	local fight = parameter.fight -- boolean
	
	-- TODO validation
	
	-- pick the spring command implementing the move
	if (not self.inProgress) then
		local cmdID = CMD.MOVE
		if (fight) then cmdID = CMD.FIGHT end	
		local vel_x, vel_y, vel_z, vel_length = SpringGetUnitVelocity(unitID)
		if not vel_length then
			return FAILURE
		end
		local x, _, z = SpringGetUnitPosition(unitID)
		local wantX = path[#path].x
		local wantZ = path[#path].z
		if vel_length < 1 then
			SpringGiveOrderToUnit(unitID, CMD.STOP, {}, 0)
			local path_index_from = path_index(unitID, path)
			for i=path_index_from, #path do
				local thisUnitWantedPosition = path[i]
				SpringGiveOrderToUnit(unitID, cmdID, thisUnitWantedPosition:AsSpringVector(), {"shift"})
			end
		end
		
		local x, _, z = SpringGetUnitPosition(unitID)
		local wantX = path[#path].x
		local wantZ = path[#path].z
		if distance(x, z, wantX, wantZ) < CLOSE_DISTANCE then
			return SUCCESS
		end
		-- check success everyone is at position
		return RUNNING
	end
end	

function Reset(self)
	self.inProgress = nil
end
