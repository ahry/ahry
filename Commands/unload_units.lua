function getInfo()
	return {
		tooltip = "Load units",
		parameterDefs = {
			{ 
				name = "unloading_area", -- units to load
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "{x=,y=,z=,r=}",
			},
			{ 
				name = "unloading_unit", -- loading unit formation
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "<loading unit>",
			},
		}
	}
end

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitTransporter = Spring.GetUnitTransporter
local SpringGetUnitIsTransporting = Spring.GetUnitIsTransporting
local SpringGetUnitsInSphere = Spring.GetUnitsInSphere
local SpringGetUnitVelocity = Spring.GetUnitVelocity 
local GetGroundHeight = Spring.GetGroundHeight
---------------- functions --------------
function distance(from_x, from_z, to_x, to_z)
	local dx = from_x - to_x
	local dz = from_z - to_z
	
	return math.sqrt(dx*dx+dz*dz)
end

function is_free(x, y, z, r, unitID)
	units_in_shape = SpringGetUnitsInSphere(x, y, z, r)
	
	for i=1, #units_in_shape do
		if units_in_shape[i] ~= unitID then
			return false
		end
	end
	return true
end

----------------------- main function -----------

function Run(self, units, parameter)
	local unloading_area = parameter.unloading_area -- table with unitID
	local unloading_unit = parameter.unloading_unit -- unitID
	
	
		
	-- validation
	if unloading_area == nil then
		Logger.warn("Ahry.load_unit", "Unloading area cannot be nil.") 
		return FAILURE		
	end
	local unitDefId = Spring.GetUnitDefID(unloading_unit)
	if not UnitDefs[unitDefId] or not UnitDefs[unitDefId].isTransport then
		Logger.warn("Ahry.load_unit", "Unit [" .. unloading_unit .. "] is not transport.") 
		return FAILURE
	end
	
	-- pick the spring command implementing the unload unit
	local cmdID = CMD.UNLOAD_UNITS
	
	area = {unloading_area.x, unloading_area.y, unloading_area.z, unloading_area.r}
	
	if #SpringGetUnitIsTransporting(unloading_unit) > 0 then
		local x,y,z = SpringGetUnitPosition(unloading_unit)
		
		local vel_x, vel_y, vel_z, vel_length = SpringGetUnitVelocity(unloading_unit)
		local height = GetGroundHeight(x,z)
		if vel_length < 1 then
			
			if distance(area[1], area[3], x, y) > area[4] or not is_free(x, height, y, 25, unloading_unit) or math.abs(vel_y) <= 0.01 then 
				SpringGiveOrderToUnit(unloading_unit, cmdID, area, {})
			end
		end
		return RUNNING
	end
	
	return SUCCESS
end