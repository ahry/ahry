function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move to defined position",
		parameterDefs = {
			{ 
				name = "positions", -- units to positions in formation
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "<units to positions>",
			},
			{ 
				name = "formation", -- absolute position formation
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "<map position formation>",
			},
			{ 
				name = "close_distance", -- absolute position formation
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "nil",
			},
			{ 
				name = "fight",
				variableType = "expression",
				componentType = "checkBox",
				defaultValue = "false",
			}
		}
	}
end


local CLOSE_DISTANCE = 200

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit


--- functions
function distance(from_x, from_z, to_x, to_z)
	local dx = from_x - to_x
	local dz = from_z - to_z
	
	return math.sqrt(dx*dx+dz*dz)
end

function Run(self, units, parameter)
	if #units == 0 then
		return SUCCESS
	end
	local positions = parameter.positions -- table
	local close_distance = parameter.close_distance or CLOSE_DISTANCE -- number
	local formation = parameter.formation -- array of Vec3
	local fight = parameter.fight -- boolean
	
	
	
	
	-- pick the spring command implementing the move
	local cmdID = CMD.MOVE
	if (fight) then cmdID = CMD.FIGHT end	
	
	for unitID, position in pairs(positions) do
		-- validation
		if Sensors.ahry.unit_alive(unitID) then
			if not positions[unitID] then
				Logger.warn("Ahry.move", "For unit [" .. unitID .. "] is not defined position [" .. dump(positions) .. "]") 
				return FAILURE
			end
			if not formation[position] then
				Logger.warn("Ahry.move", "For position [" .. unitID .. "] is not defined formation point [" .. dump(formation) .. "]") 
				return FAILURE
			end
			
			local thisUnitWantedPosition = formation[position]
			SpringGiveOrderToUnit(unitID, cmdID, thisUnitWantedPosition:AsSpringVector(), {})
		end
	end
	
	-- check success everyone is at position
	for unitID, position in pairs(positions) do
		if Sensors.ahry.unit_alive(unitID) then
			local x, _, z = SpringGetUnitPosition(unitID)
			local wantX = formation[position].x
			local wantZ = formation[position].z
			
			if distance(x, z, wantX, wantZ) > close_distance and not Sensors.ahry.position_occupied(formation[position]) then
				return RUNNING
			end
		end
	end
	return SUCCESS
end