-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

function getInfo()
    return {
        onNoUnits = SUCCESS, -- instant success
        tooltip = "Get ifno unit in param",
        parameterDefs = {
            { 
                name = "lineName",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "GoodTop",
            },
        }
    }
end

function Run(self, units, parameter)
	if Sensors.ahry.can_upgrade(parameter.lineName) then
		message.SendRules({
			subject = "swampdota_upgradeLine",
			data = {
				lineName = parameter.lineName,
				upgradeLevel = 1,
			},
		})
	end

    return SUCCESS
end


function Reset(self)
    return self
end