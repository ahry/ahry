function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move to defined position",
		parameterDefs = {
			{ 
				name = "units_to_move", -- units to positions in formation
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "<units to positions>",
			},
			{ 
				name = "pos",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "{x = 0, z = 0}",
			},
			{ 
				name = "close_distance", -- when end command
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "nil",
			},
			{ 
				name = "fight",
				variableType = "expression",
				componentType = "checkBox",
				defaultValue = "false",
			}
		}
	}
end

local CLOSE_DISTANCE = 30

---------------- functions --------------
function distance(from_x, from_z, to_x, to_z)
	local dx = from_x - to_x
	local dz = from_z - to_z
	
	return math.sqrt(dx*dx+dz*dz)
end

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

function Run(self, units, parameter)
	local units_to_move = parameter.units_to_move -- table
	local pos = parameter.pos -- table {x, z}
	if #units_to_move == 0 or not pos then
		return SUCCESS
	end
	
	
	local fight = parameter.fight -- boolean
	local close_distance = parameter.close_distance or CLOSE_DISTANCE -- number
	
	-- TODO validation
	
	
	-- pick the spring command implementing the move
	local cmdID = CMD.MOVE
	if (fight) then cmdID = CMD.FIGHT end	
	
	for i=1,#units_to_move do
		local unitID = units_to_move[i]
		local thisUnitWantedPosition = Vec3(pos.x, 0, pos.z)
		SpringGiveOrderToUnit(unitID, cmdID, thisUnitWantedPosition:AsSpringVector(), {})
	end
	
	-- check success everyone is at position
	for i=1,#units_to_move do
		local unitID = units_to_move[i]
		local x, _, z = SpringGetUnitPosition(unitID)
		local wantX = pos.x
		local wantZ = pos.z
		if not self:UnitIdle(unitID) and distance(x, z, wantX, wantZ) > close_distance then
			return RUNNING
		end
	end
	return SUCCESS
end