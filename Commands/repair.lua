function getInfo()
	return {
		tooltip = "Load units",
		parameterDefs = {
			{ 
				name = "repairing_area", -- units to load
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "{x=,y=,z=,r=}",
			},
			{ 
				name = "fark_units", -- loading unit formation
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "{<fark_unit>}",
			},
		}
	}
end

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitTransporter = Spring.GetUnitTransporter
local SpringGetUnitIsTransporting = Spring.GetUnitIsTransporting
local SpringGetUnitsInSphere = Spring.GetUnitsInSphere
local SpringGetUnitVelocity = Spring.GetUnitVelocity 
local GetGroundHeight = Spring.GetGroundHeight
local SpringGetUnitDefID = Spring.GetUnitDefID
---------------- functions --------------
function distance(from_x, from_z, to_x, to_z)
	local dx = from_x - to_x
	local dz = from_z - to_z
	
	return math.sqrt(dx*dx+dz*dz)
end

function is_free(x, y, z, r, unitID)
	units_in_shape = SpringGetUnitsInSphere(x, y, z, r)
	
	for i=1, #units_in_shape do
		if units_in_shape[i] ~= unitID then
			return false
		end
	end
	return true
end

----------------------- main function -----------

function Run(self, units, parameter)
	local repairing_area = parameter.repairing_area -- table with unitID
	local repairing_units = parameter.fark_units -- unitID
	
	
		
	-- validation
	if repairing_area == nil then
		Logger.warn("Ahry.repair", "repairing_area cannot be nil.") 
		return FAILURE		
	end
	if repairing_units == nil then
		Logger.warn("Ahry.repair", "repairing_units cannot be nil.") 
		return FAILURE		
	end
	for i=1, #repairing_units do
		local unitID = repairing_units[i]
		if unitID and Sensors.ahry.unit_alive(unitID) then
			Spring.Echo(unitID)
			local unitDefId = SpringGetUnitDefID(unitID)
			Spring.Echo(unitDefId)
			if unitDefId and (not UnitDefs[unitDefId] or UnitDefs[unitDefId].name ~= "armfark") then
				Logger.warn("Ahry.repair", "Unit [" .. unitID .. "] is not fark. It is " .. UnitDefs[unitDefId].name) 
				return FAILURE
			end
		end
	end
	
	-- pick the spring command implementing the unload unit
	local cmdID = CMD.REPAIR
	
	area = {repairing_area.x, repairing_area.y, repairing_area.z, repairing_area.r}
	local ordered_unit = nil
	for i=1, #repairing_units do
		local unitID = repairing_units[i]
		if Sensors.ahry.unit_alive(unitID) then
			ordered_unit = unitID
			SpringGiveOrderToUnit(unitID, cmdID, area, {})
		end
	end
	
	if not ordered_unit or self:UnitIdle(ordered_unit) then
		return SUCCESS
	end
	return RUNNING
end
