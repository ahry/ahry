function getInfo()
	return {
		tooltip = "Reclaim area",
		parameterDefs = {
			{ 
				name = "reclaiming_area", -- units to load
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "{x=,y=,z=,r=}",
			},
			{ 
				name = "fark_units", -- loading unit formation
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "{<farkUnit>}",
			},
		}
	}
end

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitTransporter = Spring.GetUnitTransporter
local SpringGetUnitIsTransporting = Spring.GetUnitIsTransporting
local SpringGetUnitsInSphere = Spring.GetUnitsInSphere
local SpringGetUnitVelocity = Spring.GetUnitVelocity 
local GetGroundHeight = Spring.GetGroundHeight
---------------- functions --------------
function isAreaSame(params, area)
	
	if #params + 1 ~= #area then
		return false
	end
	
	for i=1,#area do
		if area[i] ~= params[i+1] then
			return false
		end
	end
	
	return true
end
----------------------- main function -----------

function Run(self, units, parameter)
	local reclaiming_area = parameter.reclaiming_area -- table with unitID
	local fark_units = parameter.fark_units -- unitID
	
	-- TODO zjistit proč to funguje pomalu/přerušovaně... chyba v isAreaSame?
		
	-- validation
	if reclaiming_area == nil then
		Logger.warn("Ahry.reclaim", "reclaiming_area cannot be nil.") 
		return FAILURE		
	end
	for i=1, #fark_units do
		local fark = fark_units[i]
		local unitDefId = Spring.GetUnitDefID(fark)
		if not UnitDefs[unitDefId] or UnitDefs[unitDefId].name ~= "armfark" then
			Logger.warn("Ahry.reclaim", "Unit [" .. fark .. "] is not fark. It is " .. UnitDefs[unitDefId].name) 
			return FAILURE
		end
	end
	
	-- pick the spring command implementing the unload unit
	local cmdID = CMD.RECLAIM
	
	local area = {reclaiming_area.x, reclaiming_area.y, reclaiming_area.z, reclaiming_area.r}
	
	local features = Spring.GetFeaturesInSphere(reclaiming_area.x, reclaiming_area.y, reclaiming_area.z, reclaiming_area.r)
	if #features > 0 then
		for i=1, #fark_units do
			local fark = fark_units[i]
			if Sensors.ahry.unit_alive(fark) then
				local command = Spring.GetUnitCommands(fark, 1)
				local command = command and command[1]
				
				if (not command or command.id ~= CMD.RECLAIM or not isAreaSame(command.params, area)) then
					SpringGiveOrderToUnit(fark, cmdID, area, {})
				end
			end
		end
		return RUNNING
	end
	
	return SUCCESS
end