local sensorInfo = {
	name = "Slice table",
	desc = "Return first x values in tabel",
	author = "Vojtech Sejkora",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end


----------------------------------- speedups ---------------------------------------------
local CanUnitShoot = Sensors.ahry.can_unit_shoot
local GetEnemyUnits = Sensors.core.EnemyUnits
local GetUnitDefID = Spring.GetUnitDefID
local SpringGetMyTeamID = Spring.GetMyTeamID
local SpringGetUnitDefID = Spring.GetUnitDefID


----------------------------------- main functions ----------------------------------------


return function(units, name)
	local filtered_units = {}
	local filtered_units_len = 0
	for i=1,#units do
		local unitID = units[i]
		if Sensors.ahry.unit_alive(unitID) then
			filtered_units_len = filtered_units_len + 1
			filtered_units[filtered_units_len] = unitID
		end
	end
	return filtered_units
end