local sensorInfo = {
	name = "Units ids",
	desc = "Return ids's  of unit with proper name",
	author = "Vojtech Sejkora",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups
local SpringGetUnitIdDead = Spring.GetUnitIsDead
local SpringGetAllUnits = Spring.GetAllUnits
local SpringGetTeamUnits = Spring.GetTeamUnits

-- @description return id of commander
-- @return number unitId | nil
return function(name, teamID)
	local allUnits = {}
	if not teamID then
		allUnits = SpringGetAllUnits() -- table
	else
		allUnits = SpringGetTeamUnits(teamID) -- table
	end
	local ids = {}
	for _, id in pairs(allUnits) do
		unitDefId = Spring.GetUnitDefID(id)
		if UnitDefs[unitDefId] and UnitDefs[unitDefId].name == name and not SpringGetUnitIdDead(id) then
			ids[#ids+1] = id
		end
	end
	return ids
end
