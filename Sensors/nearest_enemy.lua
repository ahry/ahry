local sensorInfo = {
	name = "Commander id",
	desc = "Return commander's unit id",
	author = "Vojtech Sejkora",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups

-- @description return id of commander
-- @return number unitId | nil
return function(to_unit)
	if to_unit then
		local enemyUnit = Spring.GetUnitNearestEnemy(to_unit)
	
		return enemyUnit
	end
	return nil
end
