local sensorInfo = {
	name = "Slice table",
	desc = "Return first x values in tabel",
	author = "Vojtech Sejkora",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end
--- speedups

---------------- functions --------------
function distance(from, to)
	local dx = from.x - to.x
	local dy = from.y - to.y
	local dz = from.z - to.z
	
	return math.sqrt(dx*dx+dy*dy+dz*dz)
end


-- main
return function(from_position, list_of_positions)
	
	local nearest_position = nil
	local nearest_distance = 1
	
	for i=1, #list_of_positions do
		local position = list_of_positions[i]
		local dist = distance(from_position, position)
		if not nearest_position or dist < nearest_distance then
			nearest_distance = dist
			nearest_position = position
		end
	end
	
	return nearest_position
end
