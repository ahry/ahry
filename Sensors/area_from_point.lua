local sensorInfo = {
	name = "Dead body",
	desc = "Remember where deab robots are",
	author = "Vojtech Sejkora",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end
--- speedups
local AhryUnitAlive = Sensors.ahry.unit_alive
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGetTeamUnits = Spring.GetTeamUnits
local SpringGetTeamList = Spring.GetTeamList
local CanUnitShoot = Sensors.ahry.can_unit_shoot
local GetEnemyUnits = Sensors.core.EnemyUnits
local GetUnitDefID = Spring.GetUnitDefID
local SpringGetMyTeamID = Spring.GetMyTeamID
local SpringGetUnitDefID = Spring.GetUnitDefID



-- main
return function(point, radius)
	return {x=point.x, y=point.y, z=point.z, r=radius}
end
