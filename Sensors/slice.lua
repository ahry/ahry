local sensorInfo = {
	name = "Slice table",
	desc = "Return first x values in tabel",
	author = "Vojtech Sejkora",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- main
return function(data, from, to)
	local new_table = {}
	local index = 1
	for key, value in pairs(data) do
		if index > to then
			break
		end
		if index >= from then
			new_table[key] = value
		end
		index = index + 1
	end
	return new_table
end
