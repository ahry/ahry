local sensorInfo = {
	name = "Slice table",
	desc = "Return first x values in tabel",
	author = "Vojtech Sejkora",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end
--- speedups
local CoreMisionInfo = Sensors.core.MissionInfo
local SpringGetTeamUnitsSorted = Spring.GetTeamUnitsSorted
local SpringGetUnitsInSphere = Spring.GetUnitsInSphere
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGetMyTeamID = Spring.GetMyTeamID

local RADIUS = math.min(Sensors.ahry.max_weapon_distance(), 1000)
---------------- functions --------------
function distance(from, to)
	local dx = from.x - to.x
	local dy = from.y - to.y
	local dz = from.z - to.z
	
	return math.sqrt(dx*dx+dy*dy+dz*dz)
end


-- main
return function(point, flying_height)
	local navpoint = Vec3(point.x, point.y + flying_height, point.z)
	local enemy_units = SpringGetUnitsInSphere(point.x, point.y + flying_height, point.z, RADIUS, Spring.ENEMY_UNITS)
	for _, enemy_id in pairs(enemy_units) do
		unitDefId = Spring.GetUnitDefID(enemy_id)
		if UnitDefs[unitDefId] ~= nil then
			local can_shoot = Sensors.ahry.can_unit_shoot(enemy_id, point, flying_height > 0)
			if can_shoot ~= nil and can_shoot then
				return false
			end
		end
	end
	return true
end
