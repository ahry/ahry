local sensorInfo = {
	name = "Slice table",
	desc = "Return first x values in tabel",
	author = "Vojtech Sejkora",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end
--- speedups
local CoreMisionInfo = Sensors.core.MissionInfo
local SpringGetTeamUnitsSorted = Spring.GetTeamUnitsSorted
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGetMyTeamID = Spring.GetMyTeamID

---------------- functions --------------


-- main
return function(line_name, wanted_reserve)
	wanted_reserve = wanted_reserve or 0
	local myTeamID = Spring.GetMyTeamID()
	
	local info = CoreMisionInfo()
	local lastStronghold = Sensors.ahry.last_stronghold(string.gsub(line_name,"Good",""))
	if lastStronghold.ownerAllyID ~= Spring.GetMyAllyTeamID() then
		local metal = Spring.GetTeamResources(myTeamID,"metal")
		local spawn_size = info.battleLines[line_name].extraSpawnSize
		local cost = info.upgrade.line + wanted_reserve
		local max_size = info.upgrade.lineMaxLevel
		return metal > cost and spawn_size < max_size
	end
	return false
end
