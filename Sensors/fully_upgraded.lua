local sensorInfo = {
	name = "Slice table",
	desc = "Return first x values in tabel",
	author = "Vojtech Sejkora",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end
--- speedups
local CoreMisionInfo = Sensors.core.MissionInfo
local SpringGetTeamUnitsSorted = Spring.GetTeamUnitsSorted
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGetMyTeamID = Spring.GetMyTeamID

---------------- functions --------------


-- main
return function(line_name)
	local info = CoreMisionInfo()
	local lastStronghold = Sensors.ahry.last_stronghold(string.gsub(line_name,"Good",""))
	local spawn_size = info.battleLines[line_name].extraSpawnSize
	local max_size = info.upgrade.lineMaxLevel
	return lastStronghold.ownerAllyID == Spring.GetMyAllyTeamID() or spawn_size == max_size
end
