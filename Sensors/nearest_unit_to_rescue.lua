local sensorInfo = {
	name = "Slice table",
	desc = "Return first x values in tabel",
	author = "Vojtech Sejkora",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end
--- speedups
local CoreMisionInfo = Sensors.core.MissionInfo
local SpringGetTeamUnitsSorted = Spring.GetTeamUnitsSorted
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGetMyTeamID = Spring.GetMyTeamID
local SpringGetUnitIdDead = Spring.GetUnitIsDead

---------------- functions --------------
function distance(from, to)
	local dx = from.x - to.x
	local dy = from.y - to.y
	local dz = from.z - to.z
	
	return math.sqrt(dx*dx+dy*dy+dz*dz)
end


-- main
return function(unitID)
	local mission_info = CoreMisionInfo()
	
	local my_units = SpringGetTeamUnitsSorted(SpringGetMyTeamID())
	
	local nearest_unit = nil
	local distance_to_unit = 1
	
	global.transport = global.transport or {}
	
	local x,y,z = SpringGetUnitPosition(unitID)
	local to = Vec3(x,y,z)
	for unit_def_ID, unit_IDs in pairs(my_units) do
		local def = UnitDefs[unit_def_ID]
		if def and not def.cantBeTransported	 then
			for i=1, #unit_IDs do
				local newUnitID = unit_IDs[i]
				if not global.transport[newUnitID] or global.transport[newUnitID] == unitID or SpringGetUnitIdDead(global.transport[newUnitID]) then	
					x,y,z = SpringGetUnitPosition(newUnitID)
					local from = Vec3(x,y,z)		
					local new_distance_to_unit = distance(from, to)
					if distance(from, mission_info.safeArea.center) > mission_info.safeArea.radius and (not nearest_unit or new_distance_to_unit < distance_to_unit) then
						nearest_unit = newUnitID
						distance_to_unit = new_distance_to_unit
					end
				end
			end
		end
	end
	
	return nearest_unit
end
