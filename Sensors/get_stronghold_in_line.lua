local sensorInfo = {
	name = "Sord units ids",
	desc = "Return table where [unitId]=order number",
	author = "Vojtech Sejkora",
	date = "2019-04-20",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups
local CoreMisionInfo = Sensors.core.MissionInfo

-- @description Return table of Vec3 
-- @params around - Vec3
-- @params r - number radius of formation
-- @params number - how many point on circle
-- @return table | empty table
return function(lineName, strongholdNumber)
	local info = CoreMisionInfo()
	local linePoints = info.corridors[lineName].points
	local strongpoints = 0

	for i=1, #linePoints do
		local point = nil
		if strongholdNumber >= 0 then
			point = linePoints[i]
		else
			point = linePoints[#linePoints-i+1]
		end
		if point.isStrongpoint then
			strongpoints = strongpoints + 1
		end
		
		if strongpoints == math.abs(strongholdNumber) then
			return point
		end
	end	
	
	return nil
end
