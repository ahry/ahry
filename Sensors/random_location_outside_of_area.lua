local sensorInfo = {
	name = "Slice table",
	desc = "Return first x values in tabel",
	author = "Vojtech Sejkora",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function(area)
	local random_angle = math.random() * 2 * math.pi
	x = math.sin(random_angle) * area.r*1.2 + area.x
	z = math.sin(random_angle) * area.r*1.2 + area.z
	y = area[2]
	return Vec3(x, y, z)
end