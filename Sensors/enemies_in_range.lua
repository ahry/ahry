local sensorInfo = {
	name = "Commander id",
	desc = "Return commander's unit id",
	author = "Vojtech Sejkora",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching


local GetUnitDefID = Spring.GetUnitDefID
function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups

-- @description return id of commander
-- @return number unitId | nil
return function(unitID)
	local range = Sensors.ahry.enemy_unit_range(unitID) -- it works also for my units
	local unit_pos = range.position

	return Sensors.ahry.enemies_in_area({x=unit_pos.x, z=unit_pos.z, y=unit_pos.y, r=range})
end
