local sensorInfo = {
	name = "Units ids",
	desc = "Return ids's  of unit with proper name",
	author = "Vojtech Sejkora",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

local CACHE_TIME = 0.5 -- in seconds
function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-------------------------------------- steedups -------------
local getPosition = Sensors.ahry.position
local getRange = Sensors.ahry.enemy_unit_range
local GetGroundHeight = Spring.GetGroundHeight


---------------------------------------- functions ---------------------
return function()
	global.reserved = global.reserved or {}
	global.free_units = global.free_units or {}
	
	
	local freeUnits = {}

	local myUnitIDs = Spring.GetTeamUnits(Spring.GetMyTeamID())
	for i=1, #myUnitIDs do
		local myUnitID = myUnitIDs[i]
		if not global.reserved[myUnitID] then
			global.free_units[myUnitID] = "free"
			freeUnits[#freeUnits + 1] = myUnitID
		end
	end
	
	
	
	return freeUnits
	
	
end