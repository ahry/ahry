local sensorInfo = {
	name = "Sord units ids",
	desc = "Return table where [unitId]=order number",
	author = "Vojtech Sejkora",
	date = "2019-04-20",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups
local GetGroundHeight = Spring.GetGroundHeight

--- function
local is_int = function(n)
  return (type(n) == "number") and (math.floor(n) == n)
end

-- @description Return table of Vec3 
-- @params around - Vec3
-- @params r - number radius of formation
-- @params number - how many point on circle
-- @return table | empty table
return function(start_point, end_point, number)
	
	if number <= 0 then
		return {}
	end
	
	
	local formation = {}
	
	local dx = (end_point.x - start_point.x) / (number)
	local dz = (end_point.z - start_point.z) / (number)
	for i=1, number do
		local x = dx*(i-1) + start_point.x
		local z = dz*(i-1) + start_point.z
		position = Vec3(x, GetGroundHeight(x,z), z)
		formation[i] = position
	end	
	
	return formation
end
