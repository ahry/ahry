local sensorInfo = {
	name = "Sord units ids",
	desc = "Return table where [unitId]=order number",
	author = "Vojtech Sejkora",
	date = "2019-04-20",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups

local is_int = function(n)
  return (type(n) == "number") and (math.floor(n) == n)
end

-- @description Return table where [unitId]=order number
-- @return table | empty table
return function(units_to_sort)
	units_to_sort = units_to_sort or units
	if not units_to_sort or next(units_to_sort) == nil then
		return {}
	end
	local lastId = math.min(unpack(units_to_sort))
	local sorted_unit_table = {}
	
	sorted_unit_table[lastId] = 1
	
	for i=2, #units_to_sort do
		actualId = math.max(unpack(units_to_sort))
		for j=1, #units_to_sort do
			id = units_to_sort[j]
			if is_int(id) and lastId < id and id < actualId then
				actualId = id
			end
		end
		lastId = actualId
		sorted_unit_table[actualId] = i
	end
	return sorted_unit_table
end
