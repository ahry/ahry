local sensorInfo = {
	name = "Slice table",
	desc = "Return first x values in tabel",
	author = "Vojtech Sejkora",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end


----------------------------------- speedups ---------------------------------------------
local CanUnitShoot = Sensors.ahry.can_unit_shoot
local GetEnemyUnits = Sensors.core.EnemyUnits
local GetUnitDefID = Spring.GetUnitDefID
local SpringGetMyTeamID = Spring.GetMyTeamID


----------------------------------- main functions ----------------------------------------

-- @description Find path between two places where it should be safe to move (no enemy can shoot to you)
-- @param path_from_index -- int
-- @param path -- array of Vec3
-- @param local_enemy_units -- for this units it was safe
-- @return true / false -- it is still safe
return function(path_from_index, path, local_enemy_units)
	local_enemy_units = local_enemy_units or {}
	
	local all_enemy_units = GetEnemyUnits()

	local distinc_units = {}
	local distinc_units_len  = 0
	for i=1,#all_enemy_units do
		
		local unitDefId = GetUnitDefID(all_enemy_units[i])
		
		
		if unitDefID and UnitsDef[unitDefID] then
			local distinc = true
			for j=1,#local_enemy_units do
				if local_enemy_units[j] == all_enemy_units[i] then
					distinc = false
					break
				end
			end
			if distinc then
				distinc_units_len = distinc_units_len + 1
				distinc_units[distinc_units_len] = all_enemy_units[i]
			end
		end
	end
	
	local local_enemy_units_len = #local_enemy_units
	for j=1, #distinc_units do
		local can_shoot = nil
		for i=path_from_index,#path do
			can_shoot = CanUnitShoot(distinc_units[j], path[i])
			if can_shoot then
				return {local_enemy_units=local_enemy_units, safe=false}
			elseif can_shoot == nil then
				break
			end
		end
		if can_shoot ~= nil then
			local_enemy_units_len = local_enemy_units_len + 1
			local_enemy_units[local_enemy_units_len] = distinc_units[j]
		end
	end
	return {local_enemy_units=local_enemy_units, safe=true}
end