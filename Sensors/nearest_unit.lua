local sensorInfo = {
	name = "Units ids",
	desc = "Return ids's  of unit with proper name",
	author = "Vojtech Sejkora",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups
local AhryPosition = Sensors.ahry.position
local AhryNearestPosition = Sensors.ahry.nearest_position

-- @description return id of commander
-- @return number unitId | nil
return function(toUnit, unitIDs)
	local unitPositions = {}
	for i=1, #unitIDs do
		unitPositions[i] = AhryPosition(unitIDs[i])
	end
	local nearestPos = AhryNearestPosition(AhryPosition(toUnit), unitPositions)
	for i=1, #unitPositions do
		if nearestPos == unitPositions[i] then
			return unitIDs[i]
		end
	end
	return nil
end
