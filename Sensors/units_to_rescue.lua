local sensorInfo = {
	name = "Slice table",
	desc = "Return first x values in tabel",
	author = "Vojtech Sejkora",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end
--- speedups
local CoreMisionInfo = Sensors.core.MissionInfo
local SpringGetTeamUnitsSorted = Spring.GetTeamUnitsSorted
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGetMyTeamID = Spring.GetMyTeamID

---------------- functions --------------
function distance(from, to)
	local dx = from.x - to.x
	local dy = from.y - to.y
	local dz = from.z - to.z
	
	return math.sqrt(dx*dx+dy*dy+dz*dz)
end


-- main
return function()
	local mission_info = CoreMisionInfo()
	
	local my_units = SpringGetTeamUnitsSorted(SpringGetMyTeamID())
	
	local units_to_rescue = {}
	units_to_rescue_len = 0
	for unit_def_iD, unit_IDs in pairs(my_units) do
		if UnitDefs[unit_def_iD] and not UnitDefs[unit_def_iD].cantBeTransported then
			for i=1, #unit_IDs do
				local unitID = unit_IDs[i]
				local x,y,z = SpringGetUnitPosition(unitID)
				local from = Vec3(x,y,z)		
				if distance(from, mission_info.safeArea.center) > mission_info.safeArea.radius then
					units_to_rescue_len = units_to_rescue_len + 1
					units_to_rescue[units_to_rescue_len] = unitID
				end
			end
		end
	end
	
	return units_to_rescue
end
