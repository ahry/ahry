local sensorInfo = {
	name = "Units ids",
	desc = "Return ids's  of unit with proper name",
	author = "Vojtech Sejkora",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

local DEBUG = true -- need widget dbg_hill_debug.lua

local UNIT_TABLE_ANGLE = {["corflak"] = 0}
local UNIT_ANGLE_DEFAULT = -70
function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-------------------------------------- steedups -------------
local getPosition = Sensors.ahry.position
local getRange = Sensors.ahry.enemy_unit_range
local GetGroundHeight = Spring.GetGroundHeight


---------------------------------------- debug function -----------------

function debug_can_shoot(from, to)
	if DEBUG and Script.LuaUI('hill_debug') then
		Script.LuaUI.hill_debug(
			(from.x * Game.mapSizeZ + from.z) * Game.mapSizeX + (to.x * Game.mapSizeZ + to.z), -- key
			{line =  -- data
				{	
					startPos = from,
					endPos = to,
					width = 10,
					color = {r=0,g=0,b=0.5}
				}
			}
		)
	end
end

---------------------------------------- functions ---------------------
local PARAMS_OK = true
local PARAMS_CHECK_FAIL = false
local STEP = 4

function check_params(params)
	local signature = "Sensor has signature (unitID, position)."
	for name, value in pairs(params) do
		if value[1] == nil then
			Logger.warn("Ahry.can_unit_shoot", name .. " cannot be nil. \n".. signature) 
			return PARAMS_CHECK_FAIL
		end
	end
	
	return PARAMS_OK
end

function distanceSquare(from, to)
	local dx = from.x - to.x
	local dy = from.y - to.y
	local dz = from.z - to.z
	return (dx*dx)+(dy*dy)+(dz*dz)
end

function distance(from, to)
	return math.sqrt(distanceSquare(from, to))
end

function degree_to_rad(angle)
	return angle / 180 * math.pi
end

function can_shoot(from, to, unit_name)
	local s_x = to.x - from.x
	local s_y = to.y - from.y
	local s_z = to.z - from.z
			
	local norm = math.sqrt(s_x*s_x + s_z*s_z)
	local dx = s_x / norm
	local dy = s_y / norm
	local dz = s_z / norm
	
	local x = from.x
	local y = from.y
	local z = from.z
	
	local angle = UNIT_TABLE_ANGLE[unit_name] or UNIT_ANGLE_DEFAULT

	if dy <= math.sin(degree_to_rad(angle)) then -- TODO check
		return false
	else
		debug_can_shoot(from, to)
		return true
	end
	
	while distance({x=x, y=y, z=z}, to) < STEP do
		if GetGroundHeight(x, z) > y then
			return false
		end
		x = x + dx*STEP
		y = y + dy*STEP
		z = z + dz*STEP
	end
	return true
end


return function(unitID, position, toAircraft)
	if check_params({unitID={unitID}, position={position}, toAircraft={toAircraft}}) == PARAMS_CHECK_FAIL then
		return nil
	end
	
	local unit = getRange(unitID)
	if unit == nil then
		return nil
	end
	local dist = distance(unit.position, position)
	local los = unit.los
	if toAircraft then
		los = unit.air_los
	end
		
	if  dist < unit.range * 1  or dist < los then
		return can_shoot(unit.position, position, unit.name)
	end
	return false
end