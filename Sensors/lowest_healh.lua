local sensorInfo = {
	name = "Slice table",
	desc = "Return first x values in tabel",
	author = "Vojtech Sejkora",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end
--- speedups
local CoreMisionInfo = Sensors.core.MissionInfo
local SpringGetTeamUnitsSorted = Spring.GetTeamUnitsSorted
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGetMyTeamID = Spring.GetMyTeamID
local SpringGetUnitIdDead = Spring.GetUnitIsDead
local SpringGetUnitHealth = Spring.GetUnitHealth
local SpringGetUnitDefID = Spring.GetUnitDefID

---------------- functions --------------
function distance(from, to)
	local dx = from.x - to.x
	local dy = from.y - to.y
	local dz = from.z - to.z
	
	return math.sqrt(dx*dx+dy*dy+dz*dz)
end


-- main
return function(units_to_heal)
	
	local unit_to_heal = nil
	local unit_health = 1
	
	for i=1, #units_to_heal do
		local unitID = units_to_heal[i]
		local health = SpringGetUnitHealth(unitID)
		local unitDefId = SpringGetUnitDefID(unitID)
		if health < UnitDefs[unitDefId].health and (not unit_to_heal or unit_health > health) then
			unit_health = health
			unit_to_heal = unitID
		end
	end
	
	return unit_to_heal
end
