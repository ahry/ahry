local sensorInfo = {
	name = "Slice table",
	desc = "Return first x values in tabel",
	author = "Vojtech Sejkora",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1


local RADIUS = math.min(Sensors.ahry.max_weapon_distance(), 1000)
local ALLOW_TIME_RUN = 0.300; -- in second max is .900 because it have to end before one second running
local STEP = 64
local DEBUG = true -- need widget dbg_hill_debug.lua


function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end


----------------------------------- imports ---------------------------------------------
local List = VFS.Include("LuaUI/BETS/projects/ahry/List.lua")
local Heap = VFS.Include("LuaUI/BETS/projects/ahry/Heap.lua")

----------------------------------- speedups ---------------------------------------------
local SpringGetUnitsInSphere = Spring.GetUnitsInSphere
local GetGroundHeight = Spring.GetGroundHeight


----------------------------------- debug functions --------------------------------------

function debug_finish(point)
	if DEBUG and Script.LuaUI('hill_debug') and point then
		Script.LuaUI.hill_debug(
			point.x * Game.mapSizeZ + point.z, -- key
			{circle =  -- data
				{	
					x = point.x,
					y = point.y,
					z = point.z,
					radius = 4,
					divs = 5,
					color = {r=1,g=1,b=1}
				}
			}
		)
	end
end
function debug_visited(point)
	if DEBUG and Script.LuaUI('hill_debug') and point then
		local data = {circle =  -- data
				{	
					x = point.x,
					y = point.y,
					z = point.z,
					radius = 4,
					divs = 5,
					color = {r=1,g=0,b=1}
				}
			}
		Script.LuaUI.hill_debug(
			point.x * Game.mapSizeZ + point.z, -- key
			nil 
		)
	end
end

function debug_queue(point)
	if DEBUG and Script.LuaUI('hill_debug') and point then
		Script.LuaUI.hill_debug(
			point.x * Game.mapSizeZ + point.z, -- key
			{circle =  -- data
				{	
					x = point.x,
					y = point.y,
					z = point.z,
					radius = 4,
					divs = 5,
					color = {r=1,g=0,b=0}
				}
			}
		)
	end
end

function debug_path(point)
	if DEBUG and Script.LuaUI('hill_debug') and point then
		Script.LuaUI.hill_debug(
			point.x * Game.mapSizeZ + point.z, -- key
			{circle =  -- data
				{	
					x = point.x,
					y = point.y,
					z = point.z,
					radius = 4,
					divs = 5,
					color = {r=1,g=1,b=0}
				}
			}
		)
	end
end

function debug_backward_path(point)
	if DEBUG and Script.LuaUI('hill_debug') and point then
		Script.LuaUI.hill_debug(
			point.x * Game.mapSizeZ + point.z, -- key
			{circle =  -- data
				{	
					x = point.x,
					y = point.y,
					z = point.z,
					radius = 4,
					divs = 5,
					color = {r=0.5,g=0.5,b=0}
				}
			}
		)
	end
end

function debug_path(point)
	if DEBUG and Script.LuaUI('hill_debug') and point then
		Script.LuaUI.hill_debug(
			point.x * Game.mapSizeZ + point.z, -- key
			{circle =  -- data
				{	
					x = point.x,
					y = point.y,
					z = point.z,
					radius = 4,
					divs = 5,
					color = {r=0.5,g=0.5,b=0.5}
				}
			}
		)
	end
end

------------------------------------- functions ----------------------------------------
local sensor_start = os.clock()

function not_enought_time()
	return (os.clock() - sensor_start) > ALLOW_TIME_RUN
end

local PARAMS_OK = true
local PARAMS_CHECK_FAIL = false

function check_params(start, finish, flying_height)
	local signature = "Sensor has signature (start, finish, flying_height, data)."
	if start == nil then
		Logger.warn("Ahry.safe_path_find", "Start cannot be nil. \n".. signature) 
		return PARAMS_CHECK_FAIL
	end
	if finish == nil then
		Logger.warn("Ahry.safe_path_find", "Finish cannot be nil. \n".. signature) 
		return PARAMS_CHECK_FAIL
	end
	if flying_height == nil then
		Logger.warn("Ahry.safe_path_find", "Flying height cannot be nil. \n".. signature) 
		return PARAMS_CHECK_FAIL
	end
	return PARAMS_OK
end

function create_point(x, z)
	local y = GetGroundHeight(x, z)
	return Vec3(x, y, z)
end

function is_safe(point, flying_height)
	local enter = os.clock()
	local navpoint = Vec3(point.x, point.y + flying_height, point.z)
	local enemy_units = SpringGetUnitsInSphere(point.x, point.y + flying_height, point.z, RADIUS, Spring.ENEMY_UNITS)
	for _, enemy_id in pairs(enemy_units) do
		unitDefId = Spring.GetUnitDefID(enemy_id)
		if UnitDefs[unitDefId] ~= nil then
			local can_shoot = Sensors.ahry.can_unit_shoot(enemy_id, point, flying_height > 0)
			if can_shoot ~= nil and can_shoot then
				return false
			end
		end
	end
	return true
end

function is_on_map(point)
	return 0 <= point.x and point.x <= Game.mapSizeX and
		0 <= point.z and point.z <= Game.mapSizeZ
end

function distanceSquare(from, to)
	local dx = from.x - to.x
	local dy = from.y - to.y
	local dz = from.z - to.z
	return (dx*dx)+(dy*dy)+(dz*dz)
end

function distance(from, to)
	return math.sqrt(distanceSquare(from, to))
end

function comparer(first, second)
	return (first.distance_from_start + first.heuristic) > (second.distance_from_start + second.heuristic)
end



function get_moves(point, flying_height) 
	if not point then
		return {}
	end
	
	local step = {-1 * STEP, 0, 1 * STEP}
	local moves = {}
	for _, step_x in pairs(step) do
		for _, step_z in pairs(step) do
			if step_x ~= 0 or step_z ~= 0 then
				local new_point = create_point(point.x+step_x, point.z+step_z)
				
				if is_on_map(new_point) and (flying_height == 0 or math.abs(new_point.y - point.y) < flying_height) then
					moves[#moves + 1] = new_point
				end
			end
		end
	end
	return moves	
end

-- @description return hash for point to use in table. It ignore y coordinate
function point_hash(point)
	if point then
		local hash = point.x * Game.mapSizeX  + point.z
		return hash
	else
		Logger.error("Ahry.safe_path_find", debug.traceback()) 
	end
end

----------------------------------- main functions ----------------------------------------

-- @description Find path between two places where it should be safe to move (no enemy can shoot to you)
-- @param start -- Vec3
-- @param finish -- Vec3
-- @param flying_height -- number what is you heigh above ground when you moving 
-- @param data - nil | table (stored from previous call)
-- @return table data - in path is stored path
return function(start, finish, flying_height, data)
	sensor_start = os.clock()
	if check_params(start, finish, flying_height) == PARAMS_CHECK_FAIL then
		return
	end
	
	start.x = math.floor(start.x)
	start.z = math.floor(start.z)
	finish.x = math.floor(finish.x)
	finish.z = math.floor(finish.z)
	
	local data = data or {}
	local predecessor = data.predecessor or {}
	local safe = data.safe or {}
	data.safe = safe
	local heap = data.heap
	
	debug_finish(finish)
	
	if heap == nil then
		heap = Heap.new(comparer)
		data.heap = heap
		heap:Insert({point=start, distance_from_start=0, heuristic=distance(start, finish), from=nil})
	end
	
	data.heap = heap
	data.predecessor = predecessor
	
	while not predecessor[point_hash(finish)] and heap:Size() > 0 do
		local processed_item = heap:Pop()
		local actual_point = processed_item.point
		-- cash safe/unsafe points
		local point_is_safe = safe[point_hash(actual_point)]
		if point_is_safe == nil then
			point_is_safe = is_safe(actual_point, flying_height)
			safe[point_hash(actual_point)] = point_is_safe
		end
		if not predecessor[point_hash(actual_point)] and point_is_safe then
			predecessor[point_hash(actual_point)] = {from=processed_item.from,move=processed_item.move}
			
			debug_visited(actual_point)
			
			if distance(actual_point, finish) < 1.3*STEP then
				predecessor[point_hash(finish)] = {from=actual_point,move=nil}
				break
			end
			local moves = get_moves(processed_item.point, flying_height)
			for _, move in pairs(moves) do
				if not predecessor[point_hash(move)] and safe[point_hash(move)] == nil then
					debug_queue(move)
					local distance_from_start = processed_item.distance_from_start + distance(actual_point, move)
					heap:Insert({point=move, distance_from_start=distance_from_start, heuristic=distance(move, finish), from=actual_point, move={x=move.x-actual_point.x, z=move.z-actual_point.z}})
				end
			end
		end
		if not_enought_time() then
			return data
		end
	end
	
	if not predecessor[point_hash(finish)] then
		data.state = "fail"
		return data
	end
	
	local backward_path = data.backward_path or {{from=finish}}
	data.backward_path = backward_path
	local next_point = backward_path[#backward_path].from
	
	while next_point ~= nil do
		debug_backward_path(next_point)
		backward_path[#backward_path+1] = predecessor[point_hash(next_point)]
		next_point = predecessor[point_hash(next_point)].from
		
		if not_enought_time() then
			return data
		end
	end
	
	
	local path = data.path or {}
	local last_path_index = data.last_path_index or #backward_path
	data.path = path
	for i=last_path_index, 1, -1 do
		last_path_index = i
		if not_enought_time() then
			data.last_path_index = last_path_index		
			return data
		end
		local before_move = backward_path[i+1] and backward_path[i+1].move
		local new_move = backward_path[i].move
		--if i < 5 or #path < 5 or not before_move or not new_move or before_move.x ~= new_move.x or before_move.z ~= new_move.z then
			path[#path + 1] = backward_path[i].from
			debug_path(path[#path])
		--end
	end
	
	data.last_path_index = last_path_index
	data.state = "done"
	return data
end