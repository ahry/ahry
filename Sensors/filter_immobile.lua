local sensorInfo = {
	name = "Dead body",
	desc = "Remember where deab robots are",
	author = "Vojtech Sejkora",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end
--- speedups
local AhryUnitAlive = Sensors.ahry.unit_alive
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGetTeamUnits = Spring.GetTeamUnits
local SpringGetTeamList = Spring.GetTeamList
local CanUnitShoot = Sensors.ahry.can_unit_shoot
local GetEnemyUnits = Sensors.core.EnemyUnits
local GetUnitDefID = Spring.GetUnitDefID
local SpringGetMyTeamID = Spring.GetMyTeamID
local SpringGetUnitDefID = Spring.GetUnitDefID



-- main
return function(unitIDs)
	local fiteredUnits = {}
	for i=1,#unitIDs do
		local unitID = unitIDs[i]
		local unitDefId = SpringGetUnitDefID(unitID)
		if unitDefId and UnitDefs[unitDefId] and UnitDefs[unitDefId].isImmobile then
			fiteredUnits[#fiteredUnits + 1] = unitID
		end
	end
	return fiteredUnits
end
