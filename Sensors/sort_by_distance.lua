local sensorInfo = {
	name = "Slice table",
	desc = "Return first x values in tabel",
	author = "Vojtech Sejkora",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

local DEBUG = false -- need widget dbg_hill_debug.lua

local ALLOW_TIME_RUN = 0.500; -- in second max is .900 because it have to end before one second running

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

function distanceSqueare(from, to)
	local dx = to.x - from.x
	local dy = to.y - from.y
	local dz = to.z - from.z
	return dx*dx+dy*dy+dz*dz
end

function get_comparer(from, desc) 
	if not desc then
		return function(first, second)
			return distanceSqueare(from, first) < distanceSqueare(from, second)
		end
	else
		return function(first, second)
			return distanceSqueare(from, first) > distanceSqueare(from, second)
		end
	end
end
-- main
-- @description return id of commander
-- @param data - table of Vec3
-- @param from - Vec3
-- @param desc - bool
-- @return number unitId | nil
return function(data, from, desc)
	table.sort(data, get_comparer(from, desc))
	return data
end
