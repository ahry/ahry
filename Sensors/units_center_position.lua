local sensorInfo = {
	name = "Slice table",
	desc = "Return first x values in tabel",
	author = "Vojtech Sejkora",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end
--- speedups
local CoreMisionInfo = Sensors.core.MissionInfo
local SpringGetTeamUnitsSorted = Spring.GetTeamUnitsSorted
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGetMyTeamID = Spring.GetMyTeamID

---------------- functions --------------


-- main
return function(unitIDs)

	if #unitIDs == 0 then
		return nil
	end
	
	local info = CoreMisionInfo()

	local position = Sensors.ahry.position(unitIDs[1])
	for i=2,#unitIDs do
		position = position + Sensors.ahry.position(unitIDs[i])
	end
	return position/#unitIDs
end
