local sensorInfo = {
	name = "Slice table",
	desc = "Return first x values in tabel",
	author = "Vojtech Sejkora",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups
local GetGroundHeight = Spring.GetGroundHeight

--- function

-- @description Return table of Vec3 
-- @params around - Vec3
-- @params r - number radius of formation
-- @params number - how many point on circle
-- @return table | empty table
return function(point, heading, number, step)
		
	local orthogonal = Vec3(heading.z, heading.y, -heading.x):GetNormal()
	
	local formation = {}
	
	for i=1, number do
		local stepWithDirection = step * math.floor(i / 2)
		
		local move = orthogonal * stepWithDirection
		if i % 2 == 1 then
			move = move*-1
		end

		local x = point.x + move.x
		local z = point.z + move.z
		
		formation[i] = Vec3(x, GetGroundHeight(x,z), z)	
	end	
	
	return formation
end
