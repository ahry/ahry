local sensorInfo = {
	name = "Commander id",
	desc = "Return commander's unit id",
	author = "Vojtech Sejkora",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching


local GetUnitDefID = Spring.GetUnitDefID
function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups

-- @description return id of commander
-- @return number unitId | nil
return function(area)
	local units = Spring.GetUnitsInCylinder(area.x, area.z, area.r)
	local enemyUnits = {}
	
	local enemyTeams = Sensors.core.EnemyTeamIDs()
	for j=1, #units do
		local unitID = units[j]
		if Sensors.ahry.unit_alive(unitID) then
			local unitTeam = Spring.GetUnitTeam(unitID)
			
			for i=1, #enemyTeams do
				local teamID = enemyTeams[i]
				if unitTeam == teamID then
					enemyUnits[#enemyUnits + 1] = unitID
					break
				end
			end
		end
	end

	return enemyUnits
end
