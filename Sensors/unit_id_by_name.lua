local sensorInfo = {
	name = "Commander id",
	desc = "Return commander's unit id",
	author = "Vojtech Sejkora",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups

-- @description return id of commander
-- @return number unitId | nil
return function(name)
	local allUnits = Spring.GetAllUnits() -- table
	for _, id in pairs(allUnits) do
		unitDefId = Spring.GetUnitDefID(id)
		if UnitDefs[unitDefId] and UnitDefs[unitDefId].name == name then
			return id;
		end
	end
	return nil
end
