local sensorInfo = {
	name = "Dead body",
	desc = "Remember where deab robots are",
	author = "Vojtech Sejkora",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end
----------------------------------- speedups ---------------------------------------------
local CanUnitShoot = Sensors.ahry.can_unit_shoot
local GetEnemyUnits = Sensors.core.EnemyUnits
local GetUnitDefID = Spring.GetUnitDefID
local SpringGetMyTeamID = Spring.GetMyTeamID
local SpringGetUnitDefID = Spring.GetUnitDefID


----------------------------------- main functions ----------------------------------------


return function(units, names)
	local filtered_units = {}
	local filtered_units_len = 0
	for i=1,#units do
		local unitID = units[i]
		local unitDefId = SpringGetUnitDefID(unitID)
		if unitDefId and UnitDefs[unitDefId] then
			local unit_name = UnitDefs[unitDefId].name
			local exclude = false
			if type(names) == "table" then
				for j=1,#names do
					local name = names[j]
					if unit_name == name then
						exclude = true
						break
					end
				end
			else
				exclude = unit_name == names
			end
			if not exclude then
				filtered_units_len = filtered_units_len + 1
				filtered_units[filtered_units_len] = unitID
			end
		end
	end
	return filtered_units
end
