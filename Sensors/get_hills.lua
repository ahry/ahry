local sensorInfo = {
	name = "Hills",
	desc = "Return hills locations",
	author = "Vojtech Sejkora",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0

local DEBUG = false -- need widget dbg_hill_debug.lua

local ALLOW_TIME_RUN = 0.500; -- in second max is .900 because it have to end before one second running

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end


-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

local Heap = VFS.Include("LuaUI/BETS/projects/ahry/Heap.lua")
local List = VFS.Include("LuaUI/BETS/projects/ahry/List.lua")
-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

-- speedups
local GetGroundHeight = Spring.GetGroundHeight

---------------------------------------------------------------- debug functions
function debug_highest(value)
	if (DEBUG and Script.LuaUI('hill_debug')) then
		Script.LuaUI.hill_debug(
			value.x_index * 4096 + value.z_index, -- key
			{circle =  -- data
				{	
					x = value.x,
					y = value.y,
					z = value.z,
					radius = 5,
					divs = 5,
					color = {r=1,g=1,b=1}
				}
			}
		)
	end
end
function debug_visited(point, points)
	if (DEBUG and Script.LuaUI('hill_debug') and point.x_index ~= point.highest.x_index and point.z_index ~= point.highest.z_index) then
		Script.LuaUI.hill_debug(
			point.x_index * 4096 + point.z_index, -- key
			{circle =  -- data
				{	
					x = points[point.x_index][point.z_index].x,
					y = points[point.x_index][point.z_index].y,
					z = points[point.x_index][point.z_index].z,
					radius = 5,
					divs = 5,
					color = {r=1,g=0,b=1}
				}
			}
		)
	end
end

function debug_point(point)
	if (DEBUG and Script.LuaUI('hill_debug')) then					
		Script.LuaUI.hill_debug(
			point.x_index * 4096 + point.z_index, -- key
			{circle =  -- data
				{	
					x = point.x,
					y = point.y,
					z = point.z,
					radius = 5,
					divs = 5,
					color = {r=0,g=1,b=0}
				}
			}
		)
	end
end

-------------------------------------------------------------------------- functions
local start = os.clock()

function not_enought_time()
	return (os.clock() - start) > ALLOW_TIME_RUN
end

function compute_points(y_min, y_max, step, data)
	local points = data["points"] or {}
		
	local last = data["points_last"] or {x=0, z=0}
	local first = last ~= nil
	
	local x_index = 0
	
	for x=last.x, Game.mapSizeX, step do
		local x_index = math.floor(x / step) + 1
		local z_start = last.z
		last.z = 0
		for z=z_start, Game.mapSizeZ, step do
			local z_index = math.floor(z / step) + 1
			data["points_last"] = {x=x, x_index=x_index,z=z, z_index=z_index}
			if not_enought_time() then
				data["points"] = points
				return data
			end
		
			local height = GetGroundHeight(x, z)
			if  y_min <= height and height <= y_max then
				debug_point({x_index=x_index, z_index=z_index, x=x, z=z, y=height})
				points[x_index] = points[x_index] or {}
				points[x_index][z_index] = {x=x, y=height, z=z}
			end
		end
	end
	
	data["points_done"] = true
	data["points"] = points
	return data
end

function get_moves(x_index, z_index, points) 
	if not points[x_index][z_index] then
		return {}
	end
	
	local step = {-1, 0, 1}
	local moves = {}
	for _, step_x in pairs(step) do
		for _, step_z in pairs(step) do
			local new_x_index = x_index+step_x
			local new_z_index = z_index+step_z
			if points[new_x_index] and points[new_x_index][new_z_index] then
				moves[#moves + 1] = {x_index=new_x_index, z_index=new_z_index}
			end
		end
	end
	return moves	
end

function get_height(x_index, z_index, points)
	if not points[x_index] or not points[x_index][z_index] then
		return 0
	end
	return points[x_index][z_index].y
end

function comparer(first, second)
	return first.y < second.y
end

function prepare_heap(data)
	local points = data["points"]
	local points_last = data["points_last"]
	local last = data["prepare_heap_last"] or {x_index=1, z_index=1}
	local heap = data["heap"] or Heap.new(comparer)
	local first = last ~= nil
	
	if not data["done_heap"] or data["done_heap"] == false then
		for x_index=last.x_index, points_last.x_index do
			for z_index=last.z_index, points_last.z_index do
				if first then
					first = false
					last.z_index = 1
				end
				
				
				data["prepare_heap_last"] = {x_index=x_index, z_index=z_index}	
				if not_enought_time() then
					data["heap"] = heap
					return data
				end
				
				if points[x_index] and points[x_index][z_index] then
					local point = points[x_index][z_index]
					heap:Insert({x_index=x_index,z_index=z_index, x = point.x, z=point.z, y=point.y})
				end
			end
		end
	end
	data["done_heap"] = true
	data["heap"] = heap
	return data
end

function spread_highest(data)
	local points = data["points"]
	local queue = data["queue"] or List.new()
	local heap = data["heap"]
	local hills = data["hills"] or {}
	
	local first = true
	
	while heap:Size() > 0 do
		if queue:size() == 0 then
			if not first and DEBUG then
				start = 0
				break
			end	
			value = heap:Pop()
			if not points[value.x_index][value.z_index].highest then
				hills[#hills + 1] = Vec3(value.x, value.y, value.z)
				queue:push_right({x_index=value.x_index, z_index=value.z_index, highest=value})
				first = false
				debug_highest(value)
			end
		end
		while queue:size() > 0 do
			local point = queue:pop_left()
			
			points[point.x_index][point.z_index].highest = point.highest
			debug_visited(point, points)
			
			local moves = get_moves(point.x_index, point.z_index, points)
			
			for _, move in pairs(moves) do
				if points[point.x_index][point.z_index].y >= points[move.x_index][move.z_index].y and not points[move.x_index][move.z_index].highest then
					points[move.x_index][move.z_index].highest = point.highest
					queue:push_right({x_index=move.x_index, z_index=move.z_index, highest=point.highest})
				end
			end
			
			if not_enought_time() then
				data["hills"] = hills
				data["queue"] = queue
				data["heap"] = heap
				return data
			end
		end
	end

	data["hills"] = hills
	data["queue"] = queue
	data["heap"] = heap
	return data
end

function compute_hills(data)
	--- add all points to max heap
	--- remove top
	--- spread that highest while you can
	--- repeat
	--- from highest create hills
	local start = os.clock()
	
	if data["done"] then
		return data
	end
	
	data = prepare_heap(data)
	if not_enought_time() then
		return data
	end
	
	data = spread_highest(data)
	if not_enought_time() then
		return data
	end
	
	data["done"] = true
	return data
end

-- main
-- @description return id of commander
-- @return number unitId | nil
return function(y_min, y_max, step, data)
	start = os.clock()
	
	local data = data or {done=false}
	data = compute_points(y_min, y_max, step, data)
	
	if not_enought_time() then
		return data
	end
	data = compute_hills(data)
	return data
end
