local sensorInfo = {
	name = "Slice table",
	desc = "Return first x values in tabel",
	author = "Vojtech Sejkora",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end
--- speedups

---------------- functions --------------


-- main
return function(ttable, value)
	for i=1,#ttable do
		if value == ttable[i] then
			return true
		end
	end
	return false
end
