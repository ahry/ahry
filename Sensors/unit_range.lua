local sensorInfo = {
	name = "Commander id",
	desc = "Return commander's unit id",
	author = "Vojtech Sejkora",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching


local GetUnitDefID = Spring.GetUnitDefID
function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups

-- @description return id of commander
-- @return number unitId | nil
return function(enemyUnit)
	local unitDefId = GetUnitDefID(enemyUnit)
	if unitDefId and UnitDefs[unitDefId] then
		local max_range = 0
		local weapon_name = ""
		local air_los = UnitDefs[unitDefId].airLosRadius
		local los = UnitDefs[unitDefId].losRadius
		for _, weapon in pairs(UnitDefs[unitDefId].weapons) do
			if WeaponDefs[weapon.weaponDef].range > max_range then
				max_range = WeaponDefs[weapon.weaponDef].range
				weapon_name = WeaponDefs[weapon.weaponDef].name
			end
		end
		return {name=UnitDefs[unitDefId].name, range=max_range, weapon_name=weapon_name, position=Sensors.ahry.position(enemyUnit), air_los=air_los, los=los}
	end
	return nil
end
