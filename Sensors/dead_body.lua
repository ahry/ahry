local sensorInfo = {
	name = "Dead body",
	desc = "Remember where deab robots are",
	author = "Vojtech Sejkora",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end
--- speedups
local AhryUnitAlive = Sensors.ahry.unit_alive
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGetTeamUnits = Spring.GetTeamUnits
local SpringGetTeamList = Spring.GetTeamList



-- main
return function()
	global.corpes = global.corpes or {}
	local corpes_len = #global.corpes
	local teams = SpringGetTeamList()
	for i=1,#teams do
		local teamID = teams[i]
		local thisTeamUnits = SpringGetTeamUnits(teamID)
		
		for j=1,#thisTeamUnits do
			local unitID = thisTeamUnits[j]
			
			if not AhryUnitAlive(unitID) then
				corpes_len = corpes_len + 1
				global.corpes[corpes_len] = SpringGetUnitPosition(unitID)
			end
		end
	end
	
	return global.corpes
end
