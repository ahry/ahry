local sensorInfo = {
	name = "Sord units ids",
	desc = "Return table where [unitId]=order number",
	author = "Vojtech Sejkora",
	date = "2019-04-20",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups

local is_int = function(n)
  return (type(n) == "number") and (math.floor(n) == n)
end

-- @description Return table of Vec3 
-- @params around - Vec3
-- @params r - number radius of formation
-- @params number - how many point on circle
-- @return table | empty table
return function(data)
	local reverse_data = {}
	local reverse_data_len = 0
	for i=#data,1, -1 do 
		reverse_data_len = reverse_data_len + 1
		reverse_data[reverse_data_len] = data[i]
	end
	return reverse_data
end
