local sensorInfo = {
	name = "Commander id",
	desc = "Return commander's unit id",
	author = "Vojtech Sejkora",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching


local GetUnitDefID = Spring.GetUnitDefID
function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups

-- @description return id of commander
-- @return number unitId | nil
return function()
	return Spring.GetMyAllyTeamID()
end
