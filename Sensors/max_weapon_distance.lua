local sensorInfo = {
	name = "Units ids",
	desc = "Return ids's  of unit with proper name",
	author = "Vojtech Sejkora",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 100000 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups

-- @description return id of commander
-- @return number unitId | nil
return function()
	local max_range = 0
	for _, weapon in pairs(WeaponDefs) do
		if weapon.range > max_range then
			max_range = weapon.range
		end
	end
	return max_range
end
