local sensorInfo = {
	name = "Slice table",
	desc = "Return first x values in tabel",
	author = "Vojtech Sejkora",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end
--- speedups
local CoreMisionInfo = Sensors.core.MissionInfo
local SpringGetTeamUnitsSorted = Spring.GetTeamUnitsSorted
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGetMyTeamID = Spring.GetMyTeamID

---------------- functions --------------
function distance(from, to)
	local dx = from.x - to.x
	local dy = from.y - to.y
	local dz = from.z - to.z
	
	return math.sqrt(dx*dx+dy*dy+dz*dz)
end


-- main
return function(attacker, enemyID)
	local range_info = Sensors.ahry.enemy_unit_range(attacker)
	local pos = Sensors.ahry.position(enemyID)
	
	return pos and distance(pos, range_info.position) < range_info.range
end
