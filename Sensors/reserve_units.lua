local sensorInfo = {
	name = "Units ids",
	desc = "Return ids's  of unit with proper name",
	author = "Vojtech Sejkora",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

local DEBUG = true -- need widget dbg_hill_debug.lua

local UNIT_TABLE_ANGLE = {["corflak"] = 0}
local UNIT_ANGLE_DEFAULT = -70
function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-------------------------------------- steedups -------------
local getPosition = Sensors.ahry.position
local getRange = Sensors.ahry.enemy_unit_range
local GetGroundHeight = Spring.GetGroundHeight


---------------------------------------- functions ---------------------
local PARAMS_OK = true
local PARAMS_CHECK_FAIL = false
local STEP = 4

function check_params(params)
	local signature = "Sensor has signature (unitIDs)."
	for name, value in pairs(params) do
		if value[1] == nil then
			Logger.warn("Ahry.reserver_units", name .. " cannot be nil. \n".. signature) 
			return PARAMS_CHECK_FAIL
		end
	end
	
	return PARAMS_OK
end

return function(unitIDs, reservedBy)
	if check_params({unitIDs={unitIDs}}) == PARAMS_CHECK_FAIL then
		return nil
	end
	
	global.reserved = global.reserved or {}
	global.free_units = global.free_units or {}
	
	for i=1, #unitIDs do
		local unitID = unitIDs[i]
		global.reserved[unitID] = reservedBy or "reserved"
		global.free_units[unitID] = nil
	end
	return global.reserved
end