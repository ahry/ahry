local sensorInfo = {
	name = "Shoot to enemy",
	desc = "Return what unit should shoot to what enemy unit",
	author = "Vojtech Sejkora",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local PARAMS_OK = true
local PARAMS_CHECK_FAIL = false
local STEP = 4

function check_params(params)
	local signature = "Sensor has signature (unitIDs, enemyIDs)."
	for name, value in pairs(params) do
		if value[1] == nil then
			Logger.warn("Ahry.shoot_to_enemy", name .. " cannot be nil. \n".. signature) 
			return PARAMS_CHECK_FAIL
		end
	end
	
	return PARAMS_OK
end

return function(unitIDs, enemyIDs)
	if check_params({unitIDs={unitIDs}, enemyIDs={enemyIDs}}) == PARAMS_CHECK_FAIL then
		return nil
	end
	local health = Spring.GetUnitHealth(unitID)
	return unitID and health and health > 0
end