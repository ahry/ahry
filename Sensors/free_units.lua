local sensorInfo = {
	name = "Slice table",
	desc = "Return first x values in tabel",
	author = "Vojtech Sejkora",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end
--- speedups
local CoreMisionInfo = Sensors.core.MissionInfo
local SpringGetTeamUnitsSorted = Spring.GetTeamUnitsSorted
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGetMyTeamID = Spring.GetMyTeamID

---------------- functions --------------


-- main
return function(unitsToBeFree)
	global.reserved = global.reserved or {}
	for i=1,#unitsToBeFree do
		local unitID = unitsToBeFree[i]
		global.reserved[unitID] = nil
	end
	return true
end
