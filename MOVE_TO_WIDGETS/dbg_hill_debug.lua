moduleInfo = {
	name = "Hill debug",
	desc = "debug lines and circles",
	author = "Ahry",
	date = "2018-04-16",
	license = "MIT",
	layer = -1,
	enabled = true
}

function widget:GetInfo()
	return moduleInfo
end


-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "stringExt")
Vec3 = attach.Module(modules, "vec3")

local spEcho = Spring.Echo
local spAssignMouseCursor = Spring.AssignMouseCursor
local spSetMouseCursor = Spring.SetMouseCursor
local spGetGroundHeight = Spring.GetGroundHeight
local spTraceScreenRay = Spring.TraceScreenRay
local spGetUnitPosition = Spring.GetUnitPosition
local glColor = gl.Color
local glRect = gl.Rect
local glTexture	= gl.Texture
local glDepthTest = gl.DepthTest
local glBeginEnd = gl.BeginEnd
local glPushMatrix = gl.PushMatrix
local glPopMatrix = gl.PopMatrix
local glTranslate = gl.Translate
local glText = gl.Text
local glLineWidth = gl.LineWidth
local glCircle = gl.DrawGroundCircle
local glLineStipple = gl.LineStipple
local glVertex = gl.Vertex
local glDrawGroundCircle = gl.DrawGroundCircle
local GL_LINE_STRIP = GL.LINE_STRIP
local TextDraw = fontHandler.Draw

local max = math.max
local min = math.min

local instances = {}

local function Update(id, data)
	instances[id] = data
end

function widget:Initialize()
	widgetHandler:RegisterGlobal('hill_debug', Update)
end

function widget:GameFrame(n)
end

function draw_line(lineData)
	if (lineData.startPos ~= nil and lineData.endPos ~= nil) then
		local function Line(a, b)
			glVertex(a[1], a[2], a[3])
			glVertex(b[1], b[2], b[3])
		end
		
		local function DrawLine(a, b)
			glLineStipple(false)
			glLineWidth(5)
			glBeginEnd(GL_LINE_STRIP, Line, a, b)
			glLineStipple(false)
		end
		
		glColor(1, 0, 0, 0.2)
		
		DrawLine(
			{
				lineData.startPos.x,
				lineData.startPos.y,
				lineData.startPos.z
			}, 
			{
				lineData.endPos.x,
				lineData.endPos.y,
				lineData.endPos.z
			}
		)
	end
end

function draw_circle(circleData)
	local color = circleData.color or {r=1, g=1, b=0}
	glColor(color.r, color.g, color.b, 0.2)
	glLineWidth(5)
	glDrawGroundCircle(circleData.x, circleData.y, circleData.z, circleData.radius, circleData.divs)
end

function widget:DrawWorld()
	for instanceKey, instanceData in pairs(instances) do		
		if instanceData.line then
			draw_line(instanceData.line)
		elseif instanceData.circle then
			draw_circle(instanceData.circle)
		end
	end
	glColor(1, 0, 0, 1)
end