local List = {}
List.__index = List
function List.new()
  newList = {first=0, last=-1}
  setmetatable(newList, List)
  return newList
end

function List:push_left(value)
  local first = self.first - 1
  self.first = first
  self[first] = value
end

function List:push_right(value)
  local last = self.last + 1
  self.last = last
  self[last] = value
end

function List:pop_left()
  local first = self.first
  if first > self.last then error("list is empty") end
  local value = self[first]
  self[first] = nil        -- to allow garbage collection
  self.first = first + 1
  return value
end

function List:pop_right()
  local last = self.last
  if self.first > last then error("list is empty") end
  local value = self[last]
  self[last] = nil         -- to allow garbage collection
  self.last = last - 1
  return value
end

function List:size()
	return self.last - self.first + 1
end

return List